const express = require("express");
const app = express();
app.use(express.json());
app.use(express.static("."));

const cors = require("cors");
app.use(cors());

app.listen(8080);

const rootRoute = require("./src/routes/rootRoute");
app.use("/api", rootRoute);


app.use('/public/img', express.static('./public/img'))

const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('./swagger/swagger.json');
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerSpec));