CREATE DATABASE db_pinterest; 

-- ----------------------------
-- Table structure for nguoi_dung
-- ----------------------------
DROP TABLE IF EXISTS `nguoi_dung`;
CREATE TABLE nguoi_dung (
	nguoi_dung_id int PRIMARY KEY AUTO_INCREMENT,
	email varchar(255),
	mat_khau varchar(255),
	ho_ten varchar(255),
	tuoi int,
	anh_dai_dien varchar(255)
);
-- ----------------------------
-- Records of nguoi_dung
-- ----------------------------
INSERT INTO nguoi_dung VALUES (0,'anhtran@gmail.com','anhtran','Tuan Anh',35,'');
INSERT INTO nguoi_dung VALUES (0,'anhkhoi@gmail.com','anhkhoi','Anh Khoi',10,'');
INSERT INTO nguoi_dung VALUES (0,'minhtri@gmail.com','minhtri','Minh Tri',5,'');
INSERT INTO nguoi_dung VALUES (0,'khanhdong@gmail.com','khanhdong','Phuong Khanh',30,'');
INSERT INTO nguoi_dung VALUES (0,'demo@gmail.com','demo','Demo',25,'');


-- ----------------------------
-- Table structure for hinh_anh
-- ----------------------------
DROP TABLE IF EXISTS `hinh_anh`;
CREATE TABLE hinh_anh (
	hinh_id int PRIMARY KEY AUTO_INCREMENT,
	ten_hinh varchar(255),
	duong_dan varchar(255),
	mo_ta varchar(255),
	nguoi_dung_id int,
	FOREIGN KEY (nguoi_dung_id) REFERENCES nguoi_dung(nguoi_dung_id)	
);
-- ----------------------------
-- Records of hinh_anh
-- ----------------------------
INSERT INTO hinh_anh VALUES (0,'Gác Mây Farmstay','','Khu nghỉ dưỡng phù hợp cho gia đình và các bạn trẻ thích phượt',1);
INSERT INTO hinh_anh VALUES (0,'Slam Dunk','','Best manga ever',1);
INSERT INTO hinh_anh VALUES (0,'Doraemon','','Tuổi thơ tôi',2);
INSERT INTO hinh_anh VALUES (0,'Oppa Huy Idol','','My idol ^^',3);
INSERT INTO hinh_anh VALUES (0,'Bangkok Siam Paragon','','Thiên đường mua sắm',4);
INSERT INTO hinh_anh VALUES (0,'Hình Demo','','Deeeeeemooooooo',5);


-- ----------------------------
-- Table structure for binh_luan
-- ----------------------------
DROP TABLE IF EXISTS `binh_luan`;
CREATE TABLE binh_luan (
	nguoi_dung_id int,
	hinh_id int,
	noi_dung varchar(255),
	ngay_binh_luan date,
	PRIMARY KEY (nguoi_dung_id, hinh_id),
	FOREIGN KEY (nguoi_dung_id) REFERENCES nguoi_dung(nguoi_dung_id),
	FOREIGN KEY (hinh_id) REFERENCES hinh_anh(hinh_id)	
);
-- ----------------------------
-- Records of binh_luan
-- ----------------------------
INSERT INTO binh_luan VALUES (1,1,'Good','2022-12-22');
INSERT INTO binh_luan VALUES (2,1,'Tuyet vời','2022-12-23');
INSERT INTO binh_luan VALUES (3,1,'Chỗ này ở thì bá cháy','2022-12-24');
INSERT INTO binh_luan VALUES (3,2,'Tui cũng thích đọc truyện này lắm','2022-12-25');
INSERT INTO binh_luan VALUES (4,3,'Nobita thầm yêu XUka ^^','2022-12-26');
INSERT INTO binh_luan VALUES (5,4,'Con mình thích chú này lắm','2022-12-27');

-- ----------------------------
-- Table structure for luu_anh
-- ----------------------------
DROP TABLE IF EXISTS `luu_anh`;
CREATE TABLE luu_anh (
	nguoi_dung_id int,
	hinh_id int,
	ngay_luu date,
	PRIMARY KEY (nguoi_dung_id, hinh_id),
	FOREIGN KEY (nguoi_dung_id) REFERENCES nguoi_dung(nguoi_dung_id),
	FOREIGN KEY (hinh_id) REFERENCES hinh_anh(hinh_id)	
);
-- ----------------------------
-- Records of luu_anh
-- ----------------------------
INSERT INTO luu_anh VALUES (1,1,'2022-12-22');
INSERT INTO luu_anh VALUES (2,3,'2023-1-14');
INSERT INTO luu_anh VALUES (1,2,'2023-2-23');