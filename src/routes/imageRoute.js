const express = require("express");
const imageRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");

const {getImage, getImageByName, getImageInfoById, getCommentInfoByImageId, getSaveStateByImageId, postCommentByImageId} = require("../controllers/imageController");
imageRoute.get("/get-image", verifyToken, getImage);
imageRoute.get("/get-image-by-name", verifyToken, getImageByName);
imageRoute.get("/get-image-info-by-id/:id", verifyToken, getImageInfoById);
imageRoute.get("/get-comment-info-by-image-id/:id", verifyToken, getCommentInfoByImageId);
imageRoute.get("/get-save-status/:id", verifyToken, getSaveStateByImageId);
imageRoute.post("/post-comment-by-image-id/:id", verifyToken, postCommentByImageId);

module.exports = imageRoute;
