const express = require('express');
const rootRoute = express.Router();
const userRoute = require('./userRoute');
const imageRoute = require('./imageRoute');

// sử dụng middleware của express
rootRoute.use("/user", userRoute);
rootRoute.use("/image", imageRoute);

module.exports = rootRoute;