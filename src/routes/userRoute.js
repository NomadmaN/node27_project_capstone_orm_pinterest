const express = require("express");
const userRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");
const multer = require("multer");
const fs = require("fs");

// configure storage options
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/img"); // set the destination folder for uploaded files
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname); // set the filename for uploaded files
  },
});

// configure upload options
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 1, // set the maximum file size to 1MB
  },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/gif"
    ) {
      cb(null, true); // accept files with the specified mimetypes
    } else {
      cb(new Error("Invalid file type."), false); // reject files with other mimetypes
    }
  },
});

// upload image
userRoute.post("/upload", upload.single("file"), (req, res) => {
  if (!req.file) {
    res.status(400).send("No file uploaded.");
  } else {
    res.status(200).json({
      filename: req.file.filename,
      mimetype: req.file.mimetype,
      size: req.file.size,
    });
  }
});

const {
  signup,
  login,
  getUserInfoByUserId,
  getImageSavedFromUser,
  getImagePostedFromUser,
  deleteImagePostedByImageId,
  postNewImageFromUser,
  updateUser,
} = require("../controllers/userController");



userRoute.post("/signup", signup);
userRoute.post("/login", login);
userRoute.get("/get-user-info/:id", verifyToken, getUserInfoByUserId);
userRoute.get("/get-image-saved/:id", verifyToken, getImageSavedFromUser);
userRoute.get("/get-image-posted/:id", verifyToken, getImagePostedFromUser);
userRoute.delete("/delete-image-posted/:id", verifyToken, deleteImagePostedByImageId);
userRoute.post("/post-new-image/:id", verifyToken, postNewImageFromUser);
userRoute.put("/user-edit/:id", verifyToken, updateUser);

module.exports = userRoute;
