// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode } = require("../config/response");
const { Op } = require("sequelize");
const { checkToken } = require("../utilities/jwtoken");

// TRANG CHỦ
const getImage = async (req, res) => {
  try {
    let data = await model.hinh_anh.findAll();
    // res.status(200).send(data);
    successCode(res, data, "Lấy dữ liệu thành công")
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const getImageByName = async (req, res) => {
  try {
    let name = req.query.ten_hinh;
    let data = await model.hinh_anh.findAll({
      where: {
        ten_hinh: {
          [Op.like]: `%${name}%`,
        },
      },
    });
    // res.status(200).send(data);
    successCode(res, data, "Lấy dữ liệu thành công")
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// TRANG CHI TIẾT
const getImageInfoById = async (req, res) => {
  try {
    let { id } = req.params;
    let dataOne = await model.hinh_anh.findOne({
      where: {
        hinh_id: id
      },
      include:["nguoi_dung"], // <- take value after "as"
    });
    if (dataOne)
            // res.status(200).send(data);
            successCode(res, dataOne, "Lấy dữ liệu thành công")
        else
            // res.status(400).send("Hình không tồn tại !");
            failCode(res, id, "Hình không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const getCommentInfoByImageId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataOne = await model.hinh_anh.findOne({
      where: {
        hinh_id: id
      },
      include:["binh_luans"], // <- take value after "as"
    });
    if (dataOne)
            // res.status(200).send(data);
            successCode(res, dataOne, "Lấy dữ liệu thành công")
        else
            // res.status(400).send("Hình không tồn tại !");
            failCode(res, id, "Hình không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const getSaveStateByImageId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataCheck = await model.luu_anh.findOne({
      where: {
        hinh_id: id,
      }
    })
    if (dataCheck) {

      let saveStatus = await model.luu_anh.findOne({
        where: {
          ngay_luu: null,
        }
      })
      if (saveStatus) {
        res.status(200).send('Chưa lưu');
      } else {
        res.status(200).send('Đã lưu');
      }
    } else {
      failCode(res, id, "Hình không tồn tại")
    }  
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const postCommentByImageId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataCheck = await model.hinh_anh.findOne({
      where: {
        hinh_id: id,
      }
    })
    console.log('dataCheck == ', dataCheck)
    if (dataCheck) {

      let { nguoi_dung_id, noi_dung, ngay_binh_luan } = req.body;

      let modelUpdated = {
        hinh_id: id,
        nguoi_dung_id,
        noi_dung,
        ngay_binh_luan,
      };
      let dataComment = await model.binh_luan.create(modelUpdated);
      console.log('dataComment == ', dataComment);
      if (dataComment) {
        res.status(200).send('Đã thêm bình luận cho hình!');
      }  
    } else {
      failCode(res, id, "Hình không tồn tại")
    }  
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getImage,
  getImageByName,
  getImageInfoById,
  getCommentInfoByImageId,
  getSaveStateByImageId,
  postCommentByImageId,
};
