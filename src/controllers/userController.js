// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode, failCode404 } = require("../config/response");
const { generateToken } = require("../utilities/jwtoken.js");
const bcrypt = require("bcrypt");
const multer = require("multer");
const fs = require("fs");

// AUTH
const signup = async (req, res) => {
  try {
    let { email, mat_khau, ho_ten, tuoi } = req.body;
    let newUser = {
      email,
      mat_khau: bcrypt.hashSync(mat_khau, 10),
      ho_ten,
      tuoi,
    };

    let data = await model.nguoi_dung.create(newUser);
    console.log("data signup == ", data);
    if (data) res.status(200).send("Đăng ký user thành công");
  } catch (err) {
    res.status(500).send("Lỗi Back end");
  }
};

const login = async (req, res) => {
  try {
    let { email, password } = req.body;
    let checkUser = await model.nguoi_dung.findOne({
      where: {
        email: email,
        // mat_khau: password,
      },
    });
    // email user có tồn tại
    console.log("check user login == ", checkUser);
    if (checkUser) {
      // kiểm tra tiếp pass
      // password: chưa dc mã hóa
      // nguoi_dung - mat_khau: đã mã hóa

      // kiem tra password
      let checkPassword = bcrypt.compareSync(password, checkUser.mat_khau);
      console.log("kiểm tra password == ", checkPassword);

      if (checkPassword) {
        let accessToken = generateToken(checkUser);
        const response = {
          "accessToken": accessToken,
        }
        successCode(res, response, "Login thành công");
      } else {
        failCode(res, { email, password }, "Mật khẩu không đúng !");
      }
    } else {
      // email ko tồn tại
      failCode404(res, { email, password }, "Email không tồn tại !");
    }
  } catch (err) {
    console.log(err);
    res.status(500).send("Lỗi Back end");
  }
};

// TRANG QUẢN LÝ ẢNH
const getUserInfoByUserId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataUser = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id
      },
      // include:["nguoi_dung"],
    });
    if (dataUser)
            successCode(res, dataUser, "Lấy dữ liệu thành công")
        else
            failCode(res, id, "User không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const getImageSavedFromUser = async (req, res) => {
  try {
    let { id } = req.params;
    let dataUser = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id
      },
      include:["luu_anhs"],
    });
    if (dataUser)
            successCode(res, dataUser, "Lấy dữ liệu thành công")
        else
            failCode(res, id, "User không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const getImagePostedFromUser = async (req, res) => {
  try {
    let { id } = req.params;
    let dataUser = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id
      },
      include:["hinh_anhs"],
    });
    if (dataUser)
            successCode(res, dataUser, "Lấy dữ liệu thành công")
        else
            failCode(res, id, "User không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const deleteImagePostedByImageId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataCheck = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id,
      },
      include:["hinh_anhs"],
    })
    if (dataCheck) {
      let { idHinhMuonXoa } = req.body;
      let dataOnDelete = await model.hinh_anh.destroy({
        where: {
          hinh_id: idHinhMuonXoa,
        },
      });
      if (dataOnDelete) {
        res.status(200).send('Xóa hình thành công!');
      } else {
        failCode(res, id, "Hình không tồn tại")
      }
    } else {
      failCode(res, id, "User không tồn tại")
    }  
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// TRANG THÊM ẢNH
const postNewImageFromUser = async (req, res) => {
  try {
    let { id } = req.params;
    let dataCheck = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id,
      },
    })
    if (dataCheck) {
      console.log('dataCheck == ', dataCheck);
      let { tenHinh, duongDan, moTa } = req.body;
      let newImage = {
        ten_hinh: tenHinh,
        duong_dan: duongDan,
        mo_ta: moTa,
        nguoi_dung_id: id,
      };
      let dataImageAdd = await model.hinh_anh.create(newImage);
      if (dataImageAdd) {
        res.status(200).send('Thêm hình thành công!');
      } else {
        failCode(res, id, "Lỗi sai thông tin thêm vào")
      }
    } else {
      failCode(res, id, "User không tồn tại")
    }  
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// CHỈNH SỬA THÔNG TIN CÁ NHÂN
const updateUser = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id==========", id);

    let dataFiltered = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id,
      },
    });
    if (dataFiltered) {
      // update user
      let { email, mat_khau, ho_ten, tuoi, anh_dai_dien } = req.body;
      // ES6 => object literal
      let modelUpdate = {
        email,
        mat_khau: bcrypt.hashSync(mat_khau, 10),
        ho_ten,
        tuoi,
        anh_dai_dien
      };

      const storage = multer.diskStorage({
        destination: (req, file, callback) => {
          callback(null, process.cwd() + '/public/img');
        },
        filename: (req, file, callback) => {
          console.log(file);
          const d = new Date();
          const newName = d.getTime() + "_" + file.originalname;
          callback(null, newName);
        },
      });

      const upload = multer({
        storage,
        limits: { fileSize: 1024 * 1024 } // set max file size to 1MB
      }).single("anh_dai_dien");

      upload(req, res, async (err) => {
        if (err) {
          console.error(err);
          res.status(500).send("Lỗi upload file");
        } else {
          if (req.file) {
            let anh_dai_dien_url = req.file.path;
            modelUpdate.anh_dai_dien = anh_dai_dien_url;
          }

          let dataUpdate = await model.nguoi_dung.update(modelUpdate, {
            where: {
              nguoi_dung_id: id,
            },
          });

          if (dataUpdate[0] == 1) {
            res.status(200).send("Cập nhật user thành công");
          } else {
            res.status(400).send("Không có gì mới để cập nhật");
          }
        }
      });
    } else {
      res.status(400).send("User không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  signup,
  login,
  getUserInfoByUserId,
  getImageSavedFromUser,
  getImagePostedFromUser,
  deleteImagePostedByImageId,
  postNewImageFromUser,
  updateUser,
};
